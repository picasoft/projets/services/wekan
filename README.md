## Wekan

Ce dossier contient les ressources nécessaires pour lancer une instance Wekan sur les serveurs de Picasoft.
Il se base sur des images pré-construites et ne nécessite donc pas de construction manuelle.

L'instance comporte une base de données MongoDB et un serveur applicatif.

Voir la [documentation pour l'administration de Wekan sur le Wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:wekan:start).
Voir aussi [le README de filter-hooks](./filter-hooks) pour le code permettant de filter les hooks par activité.

### Configuration

La configuration se fait essentiellement via le [docker-compose.yml](./docker-compose.yml), au niveau des variables d'environnement.

Une fois lancé, une configuration supplémentaire est accessible via l'interface d'administration (voir Wiki).

### Lancement

Copier `mail.secrets.example` dans `mail.secrets` et remplacer le mot de passe. Il s'agit du mot de passe du compte LDAP `wekan` disponible dans le [pass](https://gitlab.utc.fr/picasoft/interne/pass).

Il suffit d'un `docker-compose up -d`.

Notez que pour que le mail soit fonctionnel, il faut remplir une seconde fois (*sic*) les informations du mail dans le panneau administrateur.

### Mise à jour

Il suffit de mettre à jour les tags des images dans le [docker-compose.yml](./docker-compose.yml), et de vérifier au niveau de la page des [Releases](https://github.com/wekan/wekan/releases) s'il y a eu des modifications.

Depuis le Panneau d'Administration, l'onglet `Annonce` permet d'envoyer un message à tous les utilisateurs connectés pour prévenir d'une mise à jour.

On pourra aussi consulter le Compose duadmin dépôt officiel pour effectuer les nouvelles configurations.

### Devenir administrateur

Par défaut, seul le premier utilisateur est administrateur. Pour pouvoir accès aux options d'administration, il faut demander à un administrateur ou bricoler la base de données :

```bash
$ docker-compose exec wekan-db bash
$ mongo
> use wekan
> db.users.update({"username": "votre_user"}, {$set: {"isAdmin": true}})
```

# Wekan Prometheus exporter

Afin d'exporter des métriques de Wekan pour la métrologie, on utilise cette image Docker qui se charge simplement d'éxécuter un script qui va collecter des informations via l'API Wekan et les exposer sur un endpoint HTTP.

Ce script lit les variables d'environnement suivantes pour sa configuration :

- `API_URL`: URL vers l'API de Kanban (par défaut `https://kanban.picasoft.net`)
- `EXPORTER_API_USER`: utilisateur admin sur l'API de Wekan (par défaut `admin`)
- `EXPORTER_API_PASSWORD`: mot de passe pour l'utilisateur
- `EXPORTER_COLLECT_INTERVAL`: nombre de secondes entre 2 actualisations des métriques (par défaut `60`)
- `INSTANCE_NAME`: nom de l'instance à ajouter en tag aux métriques (par exemple `kanban.picasoft.net`)
